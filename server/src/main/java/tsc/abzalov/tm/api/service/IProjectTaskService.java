package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(@Nullable String userId, @Nullable Task task);

    boolean hasData(@Nullable String userId);

    void addTaskToProjectById(@Nullable String userId, @Nullable String projectId,
                              @Nullable String taskId);

    @Nullable
    Project findProjectById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findTaskById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Task> findProjectTasksById(@Nullable String userId, @Nullable String projectId);

    void deleteProjectById(@Nullable String userId, @Nullable String id);

    void deleteProjectTasksById(@Nullable String userId, @Nullable String projectId);

    void deleteProjectTaskById(@Nullable String userId, @Nullable String projectId);

}
