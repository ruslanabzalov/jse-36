package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ISessionRepository;
import tsc.abzalov.tm.model.Session;

import java.util.ArrayList;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final List<Session> sessions = new ArrayList<>();

    @Override
    public void addSession(@NotNull Session session) {
        sessions.add(session);
    }

    @Override
    public void removeSession(@NotNull Session session) {
        sessions.removeIf(session::equals);
    }

    @Override
    @Nullable
    public Session findSession(@NotNull Session session) {
        val sessionIndex = sessions.indexOf(session);
        if (sessionIndex == -1) return null;
        return sessions.get(sessionIndex);
    }

}
