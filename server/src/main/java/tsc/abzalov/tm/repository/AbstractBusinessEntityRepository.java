package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.enumeration.Status;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.enumeration.Status.*;

public abstract class AbstractBusinessEntityRepository<T extends AbstractBusinessEntity> extends AbstractRepository<T>
        implements IBusinessEntityRepository<T> {

    @NotNull
    private final List<T> entities = findAll();

    @Override
    public long size(@NotNull final String userId) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    return userId.equals(currentUserId);
                })
                .count();
    }

    @Override
    public boolean isEmpty(@NotNull final String userId) {
        return entities.stream()
                .noneMatch(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    return userId.equals(currentUserId);
                });
    }

    @Override
    public int indexOf(@NotNull final String userId, @NotNull final T entity) {
        return entities.stream()
                .filter(anotherEntity -> {
                    @Nullable val currentUserId = anotherEntity.getUserId();
                    return userId.equals(currentUserId);
                })
                .collect(Collectors.toList())
                .indexOf(entity);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    return userId.equals(currentUserId);
                })
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @NotNull final String id) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @NotNull val currentId = entity.getId();
                    return userId.equals(currentUserId) && id.equals(currentId);
                })
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, final int index) {
        if (size(userId) == 0) return null;
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    return userId.equals(currentUserId);
                })
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    @Nullable
    public T findByName(@NotNull final String userId, @NotNull final String name) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @Nullable val currentName = entity.getName();
                    return userId.equals(currentUserId) && name.equals(currentName);
                })
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public T editById(@NotNull final String userId, @NotNull final String id,
                      @NotNull final String name, @NotNull final String description) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @NotNull val currentId = entity.getId();
                    return userId.equals(currentUserId) && id.equals(currentId);
                })
                .findFirst()
                .map(entity -> updateEntity(entity, name, description))
                .orElse(null);
    }

    @Override
    @Nullable
    public T editByIndex(@NotNull final String userId, final int index, @NotNull final String name,
                         @NotNull final String description) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @Nullable val searchedEntity = entities.get(index);
                    return userId.equals(currentUserId) && entity.equals(searchedEntity);
                })
                .findFirst()
                .map(entity -> updateEntity(entity, name, description))
                .orElse(null);

    }

    @Override
    @Nullable
    public T editByName(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @Nullable val currentName = entity.getName();
                    return userId.equals(currentUserId) && name.equals(currentName);
                })
                .findFirst()
                .map(entity -> {
                    entity.setDescription(description);
                    return entity;
                })
                .orElse(null);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.removeIf(entity -> {
            @Nullable val currentUserId = entity.getUserId();
            return userId.equals(currentUserId);
        });
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entities.removeIf(entity -> {
            @Nullable val currentUserId = entity.getUserId();
            @NotNull val currentId = entity.getId();
            return userId.equals(currentUserId) && id.equals(currentId);
        });
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entities.removeIf(entity -> {
            @Nullable val currentUserId = entity.getUserId();
            @Nullable val searchedEntity = entities.get(index);
            return userId.equals(currentUserId) && entity.equals(searchedEntity);
        });
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entities.removeIf(entity -> {
            @Nullable val currentUserId = entity.getUserId();
            @Nullable val currentName = entity.getName();
            return userId.equals(currentUserId) && name.equals(currentName);
        });
    }

    @Override
    @Nullable
    public T startById(@NotNull final String userId, @NotNull final String id) {
        return changeStatus(userId, id, TODO, IN_PROGRESS);
    }

    @Override
    @Nullable
    public T endById(@NotNull final String userId, @NotNull final String id) {
        return changeStatus(userId, id, IN_PROGRESS, DONE);
    }

    @Nullable
    private T changeStatus(@NotNull String userId, @NotNull String id,
                           @NotNull Status oldStatus, @NotNull Status newStatus) {
        return entities.stream()
                .filter(entity -> {
                    @Nullable val currentUserId = entity.getUserId();
                    @NotNull val currentId = entity.getId();
                    return userId.equals(currentUserId) && id.equals(currentId);
                })
                .findFirst()
                .map(entity -> {
                    @NotNull val status = entity.getStatus();
                    if (status.equals(oldStatus)) {
                        if (oldStatus.equals(TODO)) entity.setStartDate(LocalDateTime.now());
                        else entity.setEndDate(LocalDateTime.now());
                        entity.setStatus(newStatus);
                    }
                    return entity;
                })
                .orElse(null);
    }

    @NotNull
    private T updateEntity(@NotNull final T entity, @NotNull final String name, @NotNull final String description) {
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
