package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IRepository;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @NotNull
    private final IRepository<T> repository;

    public AbstractService(@NotNull final IRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public long size() {
        return repository.size();
    }

    @Override
    public boolean isEmpty() {
        return repository.isEmpty();
    }

    @Override
    @SneakyThrows
    public void create(@Nullable T entity) {
        entity = Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);
        repository.create(entity);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<T> entities) {
        entities = Optional.ofNullable(entities).orElseThrow(EmptyEntityException::new);
        repository.addAll(entities);
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return repository.findAll();
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findById(@Nullable String id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return repository.findById(id);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable String id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        repository.removeById(id);
    }

}
