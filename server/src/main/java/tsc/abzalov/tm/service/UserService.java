package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IHashingPropertyService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.User;

import java.util.Optional;

import static tsc.abzalov.tm.enumeration.Role.USER;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IHashingPropertyService propertyService;

    public UserService(@NotNull final IUserRepository userRepository,
                       @NotNull final IHashingPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password, @Nullable final Role role,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, role, firstName, lastName, email));
    }

    @Override
    public void create(@Nullable final String login, @Nullable final String password,
                       @Nullable final String firstName, @Nullable final String lastName,
                       @Nullable final String email) {
        create(createUser(login, password, USER, firstName, lastName, email));
    }

    @Override
    @SneakyThrows
    public boolean isUserExist(@Nullable String login, @Nullable String email) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        email = Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);

        val isExistByLogin = userRepository.findByLogin(login) != null;
        val isExistByEmail = userRepository.findByEmail(email) != null;
        return isExistByLogin && isExistByEmail;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @Nullable val searchedUser = userRepository.findByLogin(login);
        return Optional.ofNullable(searchedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editPasswordById(@Nullable String id, @Nullable String newPassword) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        newPassword = Optional.ofNullable(newPassword).orElseThrow(IncorrectCredentialsException::new);

        @Nullable val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @Nullable val editedUser = userRepository.editPassword(id, hash(newPassword, counter, salt));
        return Optional.ofNullable(editedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User editUserInfoById(@Nullable String id, @Nullable String firstName, @Nullable String lastName) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        firstName = Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);

        @Nullable val editedUser = userRepository.editUserInfo(id, firstName, lastName);
        return Optional.ofNullable(editedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void deleteByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);
        userRepository.deleteByLogin(login);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockById(@Nullable String id) {
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val lockedUnlockedUser = userRepository.lockUnlockById(id);
        return Optional.ofNullable(lockedUnlockedUser).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User lockUnlockByLogin(@Nullable String login) {
        login = Optional.ofNullable(login).orElseThrow(EmptyLoginException::new);

        @Nullable val lockedUnlockedUser = userRepository.lockUnlockByLogin(login);
        return Optional.ofNullable(lockedUnlockedUser).orElseThrow(EntityNotFoundException::new);
    }

    @SneakyThrows
    private void checkMainInfo(@Nullable String login, @Nullable String password,
                               @Nullable String firstName, @Nullable String email) {
        Optional.ofNullable(login).orElseThrow(IncorrectCredentialsException::new);
        Optional.ofNullable(password).orElseThrow(IncorrectCredentialsException::new);
        Optional.ofNullable(firstName).orElseThrow(EmptyFirstNameException::new);
        Optional.ofNullable(email).orElseThrow(EmptyEmailException::new);
    }

    @NotNull
    @SneakyThrows
    private User createUser(@Nullable String login, @Nullable String password,
                            @Nullable Role role, @Nullable String firstName,
                            @Nullable String lastName, @Nullable String email) {
        checkMainInfo(login, password, firstName, email);

        @NotNull val counter = propertyService.getPasswordCounterProperty();
        @NotNull val salt = propertyService.getPasswordSaltProperty();

        @NotNull val user = new User();
        user.setLogin(login);
        password = Optional.ofNullable(password).orElseThrow(EmptyPasswordException::new);
        user.setHashedPassword(hash(password, counter, salt));
        if (role != null) user.setRole(role);
        else user.setRole(USER);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        return user;
    }

}
