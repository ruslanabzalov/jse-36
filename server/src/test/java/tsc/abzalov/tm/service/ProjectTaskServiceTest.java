package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EntityNotFoundException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.*;

class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(this.projectRepository, this.taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final User user = new User();

    @NotNull
    private final String userId = this.user.getId();

    @NotNull
    private final Project project = new Project();

    @NotNull
    private final String projectId = this.project.getId();

    @NotNull
    private final Task task = new Task();

    @NotNull
    private final String taskId = task.getId();

    @Nullable
    private Session session;

    @BeforeEach
    void setUp() {
        this.project.setUserId(this.userId);
        this.task.setUserId(this.userId);
        this.task.setProjectId(this.projectId);

        projectRepository.create(this.project);
        taskRepository.create(this.task);
        userRepository.create(this.user);
    }

    @AfterEach
    void tearDown() {
        projectRepository.clear();
        taskRepository.clear();
        userRepository.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Index Test")
    void indexOf() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.indexOf(null, this.task)
                ),
                () -> assertThrows(
                        EmptyEntityException.class,
                        () -> projectTaskService.indexOf(this.userId, null)
                ),
                () -> assertEquals(0, projectTaskService.indexOf(this.userId, this.task))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Has Data Test")
    void hasData() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.hasData(null)
                ),
                () -> assertFalse(projectTaskService.hasData(this.userId + 1)),
                () -> assertTrue(projectTaskService.hasData(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Add Task To Project By Id Test")
    void addTaskToProjectById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.addTaskToProjectById(null, this.projectId, this.taskId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.addTaskToProjectById(this.userId, null, this.taskId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.addTaskToProjectById(this.userId, this.projectId, null)
                ),
                () -> {
                    @NotNull val newTask = new Task();
                    newTask.setUserId(this.userId);
                    taskRepository.create(newTask);
                    projectTaskService.addTaskToProjectById(this.userId, this.projectId, newTask.getId());
                    assertEquals(newTask.getProjectId(), this.projectId);
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find Project By Id Test")
    void findProjectById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findProjectById(null, this.projectId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findProjectById(this.userId, null)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> projectTaskService.findProjectById(this.userId, this.projectId + 1)
                )
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find Task By Id Test")
    void findTaskById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findTaskById(null, this.taskId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findTaskById(this.userId, null)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> projectTaskService.findTaskById(this.userId, this.taskId + 1)
                )
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find Project Tasks By Id Test")
    void findProjectTasksById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findProjectTasksById(null, this.projectId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.findProjectTasksById(this.userId, null)
                ),
                () -> {
                    @NotNull val projectTasks = projectTaskService.findProjectTasksById(this.userId, this.projectId);
                    assertNotNull(projectTasks);
                    assertEquals(1, projectTasks.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete Project By Id Test")
    void deleteProjectById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectById(null, this.projectId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectById(this.userId, null)
                ),
                () -> assertThrows(
                        EntityNotFoundException.class,
                        () -> {
                            projectTaskService.deleteProjectById(this.userId, this.projectId);
                            projectTaskService.findProjectById(this.userId, this.projectId);
                        }
                )
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete Project Tasks By Id Test")
    void deleteProjectTasksById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectTasksById(null, this.projectId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectTasksById(this.userId, null)
                ),
                () -> {
                    projectTaskService.deleteProjectTasksById(this.userId, this.projectId);
                    @NotNull val projectTasks = projectTaskService.findProjectTasksById(this.userId, this.projectId);

                    assertNotNull(projectTasks);
                    assertEquals(0, projectTasks.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete Project Task By Id Test")
    void deleteProjectTaskById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectTaskById(null, this.projectId)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectTaskService.deleteProjectTaskById(this.userId, null)
                ),
                () -> {
                    projectTaskService.deleteProjectTaskById(this.userId, this.projectId);
                    @Nullable val task = projectTaskService.findTaskById(this.userId, this.taskId);

                    assertNotNull(task);
                    assertNull(task.getProjectId());
                }
        );
    }

}