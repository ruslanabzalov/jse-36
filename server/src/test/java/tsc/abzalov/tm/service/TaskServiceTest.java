package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.TaskRepository;

import static org.junit.jupiter.api.Assertions.*;

class TaskServiceTest {

    @NotNull
    private static final String TASK_NAME = "Task Name";

    @NotNull
    private static final String TASK_DESCRIPTION = "Task Description";

    @NotNull
    private static final String NEW_TASK_NAME = "New Task Name";

    @NotNull
    private static final String NEW_TASK_DESCRIPTION = "New Task Description";

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final User user = new User();

    @NotNull
    private final String userId = user.getId();

    @NotNull
    private final Task task = new Task();

    {
        this.task.setName(TASK_NAME);
        this.task.setDescription(TASK_DESCRIPTION);
        this.task.setUserId(this.userId);
    }

    @BeforeEach
    void setUp() {
        taskRepository.create(this.task);
    }

    @AfterEach
    void tearDown() {
        taskRepository.removeById(this.userId, this.task.getId());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.size(null)
                ),
                () -> assertEquals(1, taskService.size(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.isEmpty(null)
                ),
                () -> assertFalse(taskService.isEmpty(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Index Test")
    void indexOf() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.indexOf(null, this.task)
                ),
                () -> assertThrows(
                        EmptyEntityException.class,
                        () -> taskService.indexOf(this.userId, null)
                ),
                () -> assertEquals(0, taskService.indexOf(this.userId, this.task))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findAll(null)
                ),
                () -> assertEquals(1, taskService.findAll(this.userId).size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findById(null, this.task.getId())
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findById(this.userId, null)
                ),
                () -> assertNotNull(taskService.findById(this.userId, this.task.getId()))
        );
    }

    // TODO: Изменить название аргумента index на position.
    @Test
    @Tag("Unit")
    @DisplayName("Find By Index Test")
    void findByIndex() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findByIndex(null, 1)
                ),
                () -> assertThrows(
                        IncorrectIndexException.class,
                        () -> taskService.findByIndex(this.userId, 2)
                ),
                () -> assertNotNull(taskService.findByIndex(this.userId, 1))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Name Test")
    void findByName() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.findByName(null, TASK_NAME)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.findByName(this.userId, null)
                ),
                () -> assertNotNull(taskService.findByName(this.userId, TASK_NAME))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Id Test")
    void editById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.editById(null, this.task.getId(), NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.editById(this.userId, null, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.editById(this.userId, this.task.getId(), null, NEW_TASK_DESCRIPTION)
                ),
                () -> assertNotNull(taskService.editById(this.userId, this.task.getId(), NEW_TASK_NAME, null))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Index Test")
    void editByIndex() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.editByIndex(null, 1, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        IncorrectIndexException.class,
                        () -> taskService.editByIndex(this.userId, 2, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.editByIndex(this.userId, 1, null, NEW_TASK_DESCRIPTION)
                ),
                () -> assertNotNull(taskService.editByIndex(this.userId, 1, NEW_TASK_NAME, NEW_TASK_DESCRIPTION))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Name Test")
    void editByName() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.editByName(null, NEW_TASK_NAME, NEW_TASK_DESCRIPTION)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.editByName(this.userId, null, NEW_TASK_DESCRIPTION)
                ),
                () -> assertNotNull(taskService.editByName(this.userId, TASK_NAME, NEW_TASK_DESCRIPTION))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.clear(null)
                ),
                () -> {
                    taskService.clear(this.userId);
                    assertTrue(taskService.isEmpty(this.userId));
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeById(null, this.task.getId())
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeById(this.userId, null)
                ),
                () -> {
                    taskService.removeById(this.userId, this.task.getId());
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> taskService.findById(this.userId, this.task.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Index Test")
    void removeByIndex() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeByIndex(null, 1)
                ),
                () -> assertThrows(
                        IncorrectIndexException.class,
                        () -> taskService.removeByIndex(this.userId, 2)
                ),
                () -> {
                    taskService.removeByIndex(this.userId, 1);
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> taskService.findById(this.userId, this.task.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Name Test")
    void removeByName() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.removeByName(null, TASK_NAME)
                ),
                () -> assertThrows(
                        EmptyNameException.class,
                        () -> taskService.removeByName(this.userId, null)
                ),
                () -> {
                    taskService.removeByName(this.userId, TASK_NAME);
                    assertThrows(
                            EntityNotFoundException.class,
                            () -> taskService.findById(this.userId, this.task.getId())
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Start By Id Test")
    void startById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.startById(null, this.task.getId())
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.startById(this.userId, null)
                ),
                () -> assertNotNull(taskService.startById(this.userId, this.task.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("End By Id Test")
    void endById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.endById(null, this.task.getId())
                ),
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> taskService.endById(this.userId, null)
                ),
                () -> assertNotNull(taskService.endById(this.userId, this.task.getId()))
        );
    }

}