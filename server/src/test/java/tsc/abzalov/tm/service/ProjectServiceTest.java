package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.IService;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.exception.data.EmptyEntityException;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.User;
import tsc.abzalov.tm.repository.ProjectRepository;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ProjectServiceTest {

    @NotNull
    private static final String PROJECT_NAME = "Project Name";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "Project Description";

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IService<Project> projectService = new ProjectService(this.projectRepository);

    @NotNull
    private final User user = new User();

    @NotNull
    private final String userId = this.user.getId();

    @NotNull
    private final Project project = new Project();

    @NotNull
    private final String projectId = this.project.getId();

    {
        this.project.setName(PROJECT_NAME);
        this.project.setDescription(PROJECT_DESCRIPTION);
        this.project.setUserId(this.userId);
    }

    @BeforeEach
    void setUp() {
        projectRepository.create(this.project);
    }

    @AfterEach
    void tearDown() {
        projectRepository.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertEquals(1, projectService.size());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertFalse(projectService.isEmpty());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Create Test")
    void create() {
        assertAll(
                () -> assertThrows(
                        EmptyEntityException.class,
                        () -> projectService.create(null)
                ),
                () -> {
                    @NotNull val newProject = new Project();
                    projectService.create(newProject);
                    assertEquals(2, projectService.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Add All Test")
    void addAll() {
        @NotNull val newProjectList = new ArrayList<Project>();
        for (int i = 0; i < 10; i++) newProjectList.add(new Project());

        assertAll(
                () -> assertThrows(
                        EmptyEntityException.class,
                        () -> projectService.addAll(null)
                ),
                () -> {
                    projectService.addAll(newProjectList);
                    assertEquals(10, projectService.size());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        assertEquals(1, projectService.findAll().size());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.findById(null)
                ),
                () -> assertEquals(this.project, projectService.findById(this.projectId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        projectService.clear();
        assertTrue(projectService.isEmpty());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        assertAll(
                () -> assertThrows(
                        EmptyIdException.class,
                        () -> projectService.removeById(null)
                ),
                () -> {
                    projectService.removeById(projectId);
                    assertTrue(projectService.isEmpty());
                }
        );
    }

}