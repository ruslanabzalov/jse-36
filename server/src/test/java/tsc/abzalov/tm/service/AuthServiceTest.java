package tsc.abzalov.tm.service;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.enumeration.Role.USER;

class AuthServiceTest {

    @NotNull
    private static final String LOGIN = "Login";

    @NotNull
    private static final String PASSWORD = "Password";

    @NotNull
    private static final String FIRSTNAME = "Firstname";

    @NotNull
    private static final String LASTNAME = "Lastname";

    @NotNull
    private static final String EMAIL = "Email";

    @NotNull
    private static final String LOCKED_SUFFIX = "_locked";

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IApplicationPropertyService propertyService = new ApplicationPropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @BeforeEach
    void setUp() {
        authService.register(LOGIN, PASSWORD, FIRSTNAME, LASTNAME, EMAIL);
    }

    @AfterEach
    void tearDown() {
        if (!authService.isSessionInactive()) authService.logoff();
        userRepository.deleteByLogin(LOGIN);
    }

    @Test
    @Tag("Unit")
    @DisplayName("Register Test")
    void register() {
        authService.logoff();
        userRepository.deleteByLogin(LOGIN);

        assertAll(
                () -> assertThrows(
                        EmptyLoginException.class,
                        () -> authService.register(null, PASSWORD, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        EmptyPasswordException.class,
                        () -> authService.register(LOGIN, null, FIRSTNAME, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        EmptyFirstNameException.class,
                        () -> authService.register(LOGIN, PASSWORD, null, LASTNAME, EMAIL)
                ),
                () -> assertThrows(
                        EmptyEmailException.class,
                        () -> authService.register(LOGIN, PASSWORD, FIRSTNAME, LASTNAME, null)
                ),
                () -> {
                    authService.register(LOGIN, PASSWORD, FIRSTNAME, null, EMAIL);
                    @Nullable val createdUser = userRepository.findByLogin(LOGIN);
                    assertNotNull(createdUser);
                    assertEquals(FIRSTNAME, createdUser.getFirstName());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Login Test")
    void login() {
        authService.logoff();
        authService.register(
                LOGIN + LOCKED_SUFFIX, PASSWORD + LOCKED_SUFFIX,
                FIRSTNAME, LASTNAME, EMAIL + LOCKED_SUFFIX
        );

        @Nullable val lockedUser = userRepository.lockUnlockByLogin(LOGIN + LOCKED_SUFFIX);
        assertNotNull(lockedUser);

        assertAll(
                () -> assertThrows(
                        IncorrectCredentialsException.class,
                        () -> authService.login(null, PASSWORD)
                ),
                () -> assertThrows(
                        IncorrectCredentialsException.class,
                        () -> authService.login(LOGIN, null)
                ),
                () -> assertThrows(
                        UserIsNotExistException.class,
                        () -> authService.login("NOT_EXIST", "NOT_EXIST")
                ),
                () -> assertThrows(
                        UserLockedException.class,
                        () -> authService.login(lockedUser.getLogin(), PASSWORD + LOCKED_SUFFIX)
                ),
                () -> assertThrows(
                        AccessDeniedException.class,
                        () -> authService.login(LOGIN, PASSWORD + "!")
                ),
                () -> {
                    authService.login(LOGIN, PASSWORD);
                    assertFalse(authService.isSessionInactive());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Logoff Test")
    void logoff() {
        authService.logoff();

        assertAll(
                () -> assertThrows(
                        SessionIsInactiveException.class,
                        authService::logoff
                ),
                () -> {
                    authService.login(LOGIN, PASSWORD);
                    assertFalse(authService.isSessionInactive());
                    authService.logoff();
                    assertTrue(authService.isSessionInactive());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Session Status Test")
    void isSessionInactive() {
        assertAll(
                () -> assertFalse(authService.isSessionInactive()),
                () -> {
                    authService.logoff();
                    assertTrue(authService.isSessionInactive());
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Get Current User Id Test")
    void getCurrentUserId() {
        assertAll(
                () -> assertNotNull(authService.getCurrentUserId()),
                () -> {
                    authService.logoff();
                    assertThrows(
                            SessionIsInactiveException.class,
                            authService::getCurrentUserId
                    );
                }
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Get Current User Login Test")
    void getCurrentUserLogin() {
        assertAll(
                () -> assertEquals(LOGIN, authService.getCurrentUserLogin()),
                () -> assertThrows(
                        SessionIsInactiveException.class,
                        () -> {
                            authService.logoff();
                            authService.getCurrentUserLogin();
                        }
                )
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Get Current User Role Test")
    void getCurrentUserRole() {
        assertAll(
                () -> assertEquals(USER, authService.getCurrentUserRole()),
                () -> assertThrows(
                        SessionIsInactiveException.class,
                        () -> {
                            authService.logoff();
                            authService.getCurrentUserRole();
                        }
                )
        );
    }

}