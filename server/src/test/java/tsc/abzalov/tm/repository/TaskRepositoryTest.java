package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static tsc.abzalov.tm.enumeration.Status.DONE;
import static tsc.abzalov.tm.enumeration.Status.IN_PROGRESS;

// TODO: Сравнивать исходный объект с его клоном.
class TaskRepositoryTest {

    @NotNull
    private static final String TASK_NAME = "Simple Task Name";

    @NotNull
    private final ITaskRepository emptyTasksRepository = new TaskRepository();

    @NotNull
    private final ITaskRepository smallTasksRepository = new TaskRepository();

    @NotNull
    private final ITaskRepository bigTasksRepository = new TaskRepository();

    @NotNull
    private final Task task = new Task();

    @NotNull
    private final User user = new User();

    @NotNull
    private final String userId = user.getId();

    @NotNull
    private final Project project = new Project();

    @NotNull
    private List<Task> getSmallTasksList() {
        return getConfiguredTasksList(100);
    }

    @NotNull
    private List<Task> getBigTasksList() {
        return getConfiguredTasksList(10000);
    }

    @NotNull
    private List<Task> getConfiguredTasksList(final int tasksSize) {
        @Nullable Task task;
        @NotNull val taskList = new ArrayList<Task>();
        for (int i = 0; i < tasksSize; i++) {
            task = new Task();
            task.setName("Task Name " + i);
            task.setDescription("Description " + i);
            task.setUserId(this.userId);
            taskList.add(task);
        }
        return taskList;
    }

    {
        this.task.setUserId(this.userId);
        this.task.setName(TASK_NAME);
        this.task.setProjectId(this.project.getId());
    }

    @BeforeEach
    void setUp() {
        smallTasksRepository.addAll(getSmallTasksList());
        bigTasksRepository.addAll(getBigTasksList());
    }

    @AfterEach
    void tearDown() {
        smallTasksRepository.clear();
        bigTasksRepository.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertAll(
                () -> assertEquals(0, emptyTasksRepository.size(this.userId)),
                () -> assertEquals(100, smallTasksRepository.size(this.userId)),
                () -> assertEquals(10000, bigTasksRepository.size(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertAll(
                () -> assertTrue(emptyTasksRepository.isEmpty(this.userId)),
                () -> assertFalse(smallTasksRepository.isEmpty(this.userId)),
                () -> assertFalse(bigTasksRepository.isEmpty(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Index Test")
    void indexOf() {
        smallTasksRepository.create(this.task);
        bigTasksRepository.create(this.task);

        assertAll(
                () -> assertEquals(-1, emptyTasksRepository.indexOf(this.userId, this.task)),
                () -> assertEquals(100, smallTasksRepository.indexOf(this.userId, this.task)),
                () -> assertEquals(10000, bigTasksRepository.indexOf(this.userId, this.task))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        @NotNull val emptyRepoAllTasks = emptyTasksRepository.findAll(this.userId);
        @NotNull val smallRepoAllTasks = smallTasksRepository.findAll(this.userId);
        @NotNull val bigRepoAllTasks = bigTasksRepository.findAll(this.userId);

        assertAll(
                () -> assertEquals(0, emptyRepoAllTasks.size()),
                () -> assertEquals(100, smallRepoAllTasks.size()),
                () -> assertEquals(10000, bigRepoAllTasks.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        smallTasksRepository.create(this.task);
        bigTasksRepository.create(this.task);

        assertAll(
                () -> assertNull(emptyTasksRepository.findById(this.userId, this.task.getId())),
                () -> assertEquals(this.task, smallTasksRepository.findById(this.userId, this.task.getId())),
                () -> assertEquals(this.task, bigTasksRepository.findById(this.userId, this.task.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Index Test")
    void findByIndex() {
        smallTasksRepository.create(this.task);
        bigTasksRepository.create(this.task);

        assertAll(
                () -> assertNull(emptyTasksRepository.findByIndex(this.userId, 0)),
                () -> assertEquals(this.task, smallTasksRepository.findByIndex(this.userId, 100)),
                () -> assertEquals(this.task, bigTasksRepository.findByIndex(this.userId, 10000))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Name Test")
    void findByName() {
        smallTasksRepository.create(this.task);
        bigTasksRepository.create(this.task);

        assertAll(
                () -> assertNull(emptyTasksRepository.findByName(this.userId, TASK_NAME)),
                () -> assertEquals(this.task, smallTasksRepository.findByName(this.userId, TASK_NAME)),
                () -> assertEquals(this.task, bigTasksRepository.findByName(this.userId, TASK_NAME))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Id Test")
    void editById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        @NotNull val newTaskName = "New Task Name";
        @NotNull val newTaskDescription = "New Task Description";

        @Nullable val editedTaskEmptyRepo =
                emptyTasksRepository.editById(this.userId, clonedTask.getId(), newTaskName, newTaskDescription);
        @Nullable val editedTaskSmallRepo =
                smallTasksRepository.editById(this.userId, clonedTask.getId(), newTaskName, newTaskDescription);
        @Nullable val editedTaskBigRepo =
                bigTasksRepository.editById(this.userId, clonedTask.getId(), newTaskName, newTaskDescription);

        assertNotNull(editedTaskSmallRepo);
        assertNotNull(editedTaskBigRepo);
        assertAll(
                () -> assertNull(editedTaskEmptyRepo),
                () -> assertEquals(newTaskName, editedTaskSmallRepo.getName()),
                () -> assertEquals(newTaskDescription, editedTaskSmallRepo.getDescription()),
                () -> assertEquals(newTaskName, editedTaskBigRepo.getName()),
                () -> assertEquals(newTaskDescription, editedTaskBigRepo.getDescription())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Index Test")
    void editByIndex() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        @NotNull val newTaskName = "New Task Name";
        @NotNull val newTaskDescription = "New Task Description";

        @Nullable val editedTaskEmptyRepo =
                emptyTasksRepository.editByIndex(this.userId, 0, newTaskName, newTaskDescription);
        @Nullable val editedTaskSmallRepo =
                smallTasksRepository.editByIndex(this.userId, 100, newTaskName, newTaskDescription);
        @Nullable val editedTaskBigRepo =
                bigTasksRepository.editByIndex(this.userId, 10000, newTaskName, newTaskDescription);

        assertNotNull(editedTaskSmallRepo);
        assertNotNull(editedTaskBigRepo);
        assertAll(
                () -> assertNull(editedTaskEmptyRepo),
                () -> assertEquals(newTaskName, editedTaskSmallRepo.getName()),
                () -> assertEquals(newTaskDescription, editedTaskSmallRepo.getDescription()),
                () -> assertEquals(newTaskName, editedTaskBigRepo.getName()),
                () -> assertEquals(newTaskDescription, editedTaskBigRepo.getDescription())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit By Name Test")
    void editByName() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        @NotNull val newTaskDescription = "New Task Description";

        @Nullable val editedTaskEmptyRepo =
                emptyTasksRepository.editByName(this.userId, TASK_NAME, newTaskDescription);
        @Nullable val editedTaskSmallRepo =
                smallTasksRepository.editByName(this.userId, TASK_NAME, newTaskDescription);
        @Nullable val editedTaskBigRepo =
                bigTasksRepository.editByName(this.userId, TASK_NAME, newTaskDescription);

        assertNotNull(editedTaskSmallRepo);
        assertNotNull(editedTaskBigRepo);
        assertAll(
                () -> assertNull(editedTaskEmptyRepo),
                () -> assertEquals(newTaskDescription, editedTaskSmallRepo.getDescription()),
                () -> assertEquals(newTaskDescription, editedTaskBigRepo.getDescription())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        emptyTasksRepository.clear(this.userId);
        smallTasksRepository.clear(this.userId);
        bigTasksRepository.clear(this.userId);

        assertAll(
                () -> assertTrue(emptyTasksRepository.isEmpty(this.userId)),
                () -> assertTrue(smallTasksRepository.isEmpty(this.userId)),
                () -> assertTrue(bigTasksRepository.isEmpty(this.userId))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        emptyTasksRepository.removeById(this.userId, clonedTask.getId());
        smallTasksRepository.removeById(this.userId, clonedTask.getId());
        bigTasksRepository.removeById(this.userId, clonedTask.getId());

        assertAll(
                () -> assertNull(emptyTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(smallTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(bigTasksRepository.findById(this.userId, clonedTask.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Index Test")
    void removeByIndex() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        emptyTasksRepository.removeByIndex(this.userId, 0);
        smallTasksRepository.removeByIndex(this.userId, 100);
        bigTasksRepository.removeByIndex(this.userId, 10000);

        assertAll(
                () -> assertNull(emptyTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(smallTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(bigTasksRepository.findById(this.userId, clonedTask.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Name Test")
    void removeByName() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        emptyTasksRepository.removeByName(this.userId, TASK_NAME);
        smallTasksRepository.removeByName(this.userId, TASK_NAME);
        bigTasksRepository.removeByName(this.userId, TASK_NAME);

        assertAll(
                () -> assertNull(emptyTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(smallTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(bigTasksRepository.findById(this.userId, clonedTask.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Start By Id Test")
    void startById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        @Nullable val startedTaskEmptyRepo = emptyTasksRepository.startById(this.userId, clonedTask.getId());
        @Nullable val startedTaskSmallRepo = smallTasksRepository.startById(this.userId, clonedTask.getId());
        @Nullable val startedTaskBigRepo = bigTasksRepository.startById(this.userId, clonedTask.getId());

        assertNotNull(startedTaskSmallRepo);
        assertNotNull(startedTaskBigRepo);
        assertAll(
                () -> assertNull(startedTaskEmptyRepo),
                () -> assertNotNull(startedTaskSmallRepo.getStartDate()),
                () -> assertNull(startedTaskSmallRepo.getEndDate()),
                () -> assertEquals(IN_PROGRESS, startedTaskSmallRepo.getStatus()),
                () -> assertNotNull(startedTaskBigRepo.getStartDate()),
                () -> assertNull(startedTaskBigRepo.getEndDate()),
                () -> assertEquals(IN_PROGRESS, startedTaskBigRepo.getStatus())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("End By Id Test")
    void endById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        emptyTasksRepository.startById(this.userId, clonedTask.getId());
        smallTasksRepository.startById(this.userId, clonedTask.getId());
        bigTasksRepository.startById(this.userId, clonedTask.getId());

        @Nullable val startedTaskEmptyRepo = emptyTasksRepository.endById(this.userId, clonedTask.getId());
        @Nullable val startedTaskSmallRepo = smallTasksRepository.endById(this.userId, clonedTask.getId());
        @Nullable val startedTaskBigRepo = bigTasksRepository.endById(this.userId, clonedTask.getId());

        assertNotNull(startedTaskSmallRepo);
        assertNotNull(startedTaskBigRepo);
        assertAll(
                () -> assertNull(startedTaskEmptyRepo),
                () -> assertNotNull(startedTaskSmallRepo.getStartDate()),
                () -> assertNotNull(startedTaskSmallRepo.getEndDate()),
                () -> assertEquals(DONE, startedTaskSmallRepo.getStatus()),
                () -> assertNotNull(startedTaskBigRepo.getStartDate()),
                () -> assertNotNull(startedTaskBigRepo.getEndDate()),
                () -> assertEquals(DONE, startedTaskBigRepo.getStatus())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Add Task To Project By Id Test")
    void addTaskToProjectById() {
        @NotNull val newProject = new Project();
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(clonedTask);

        smallTasksRepository.addTaskToProjectById(this.userId, newProject.getId(), clonedTask.getId());
        bigTasksRepository.addTaskToProjectById(this.userId, newProject.getId(), clonedTask.getId());

        assertEquals(newProject.getId(), clonedTask.getProjectId());
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find Project Tasks By Id Test")
    void findProjectTasksById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(this.task);
        bigTasksRepository.create(clonedTask);

        @NotNull val foundedTasksEmptyRepo =
                emptyTasksRepository.findProjectTasksById(this.userId, this.project.getId());
        @NotNull val foundedTasksSmallRepo =
                smallTasksRepository.findProjectTasksById(this.userId, this.project.getId());
        @NotNull val foundedTasksBigRepo =
                bigTasksRepository.findProjectTasksById(this.userId, this.project.getId());

        assertAll(
                () -> assertEquals(0, foundedTasksEmptyRepo.size()),
                () -> assertEquals(1, foundedTasksSmallRepo.size()),
                () -> assertEquals(2, foundedTasksBigRepo.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete Project Tasks By Id")
    void deleteProjectTasksById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(this.task);
        bigTasksRepository.create(clonedTask);

        emptyTasksRepository.deleteProjectTasksById(this.userId, this.project.getId());
        smallTasksRepository.deleteProjectTasksById(this.userId, this.project.getId());
        bigTasksRepository.deleteProjectTasksById(this.userId, this.project.getId());

        assertAll(
                () -> assertTrue(emptyTasksRepository.isEmpty(this.userId)),
                () -> assertNull(smallTasksRepository.findById(this.userId, clonedTask.getId())),
                () -> assertNull(bigTasksRepository.findById(this.userId, clonedTask.getId()))
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Delete Project Task By Id")
    void deleteProjectTaskById() {
        @Nullable val clonedTask = this.task.clone();
        assertNotNull(clonedTask);

        @Nullable val anotherClonedTask = this.task.clone();
        assertNotNull(anotherClonedTask);

        smallTasksRepository.create(clonedTask);
        bigTasksRepository.create(anotherClonedTask);

        assertNotNull(clonedTask.getProjectId());
        assertNotNull(anotherClonedTask.getProjectId());

        emptyTasksRepository.deleteProjectTaskById(this.userId, clonedTask.getProjectId());
        smallTasksRepository.deleteProjectTaskById(this.userId, clonedTask.getProjectId());
        bigTasksRepository.deleteProjectTaskById(this.userId, anotherClonedTask.getProjectId());

        @Nullable val foundedTaskSmallRepo = smallTasksRepository.findById(this.userId, clonedTask.getId());
        @Nullable val foundedTaskBigRepo = bigTasksRepository.findById(this.userId, anotherClonedTask.getId());

        assertNotNull(foundedTaskSmallRepo);
        assertNotNull(foundedTaskBigRepo);
        assertAll(
                () -> assertTrue(emptyTasksRepository.isEmpty(this.userId)),
                () -> assertNull(foundedTaskSmallRepo.getProjectId()),
                () -> assertNull(foundedTaskBigRepo.getProjectId())
        );
    }

}