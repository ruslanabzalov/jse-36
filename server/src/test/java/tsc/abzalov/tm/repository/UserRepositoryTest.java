package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static tsc.abzalov.tm.enumeration.Role.USER;

// TODO: Сравнивать исходный объект с его клоном.
class UserRepositoryTest {

    @NotNull
    private final IUserRepository emptyUserRepository = new UserRepository();

    @NotNull
    private final IUserRepository smallUserRepository = new UserRepository();

    @NotNull
    private final IUserRepository bigUserRepository = new UserRepository();

    @NotNull
    private List<User> getSmallUsersList() {
        return getConfiguredUsersList(100);
    }

    @NotNull
    private List<User> getBigUsersList() {
        return getConfiguredUsersList(10000);
    }

    @NotNull
    private List<User> getConfiguredUsersList(final int usersSize) {
        @Nullable User user;
        @NotNull val userList = new ArrayList<User>();
        for (int i = 0; i < usersSize; i++) {
            user = new User();
            user.setFirstName("Firstname " + usersSize);
            user.setLastName("Lastname " + usersSize);
            user.setLogin("Login " + usersSize);
            user.setHashedPassword("1234567890");
            user.setEmail("email" + usersSize + "@mail.com");
            user.setRole(USER);
            userList.add(user);
        }
        return userList;
    }

    @BeforeEach
    void setUp() {
        smallUserRepository.addAll(getSmallUsersList());
        bigUserRepository.addAll(getBigUsersList());
    }

    @AfterEach
    void tearDown() {
        smallUserRepository.clear();
        bigUserRepository.clear();
    }

    @Test
    @Tag("Unit")
    @DisplayName("Size Test")
    void size() {
        assertAll(
                () -> assertEquals(0, emptyUserRepository.size()),
                () -> assertEquals(100, smallUserRepository.size()),
                () -> assertEquals(10000, bigUserRepository.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Empty Test")
    void isEmpty() {
        assertAll(
                () -> assertTrue(emptyUserRepository.isEmpty()),
                () -> assertFalse(smallUserRepository.isEmpty()),
                () -> assertFalse(bigUserRepository.isEmpty())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Create Test")
    void create() {
        val oldEmptyRepoSize = emptyUserRepository.size();
        val oldSmallRepoSize = smallUserRepository.size();
        val oldBigRepoSize = bigUserRepository.size();

        @NotNull val mockUser = mock(User.class);

        emptyUserRepository.create(mockUser);
        smallUserRepository.create(mockUser);
        bigUserRepository.create(mockUser);

        assertAll(
                () -> assertEquals(oldEmptyRepoSize + 1, emptyUserRepository.size()),
                () -> assertEquals(oldSmallRepoSize + 1, smallUserRepository.size()),
                () -> assertEquals(oldBigRepoSize + 1, bigUserRepository.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Add All Test")
    void addAll() {
        emptyUserRepository.addAll(getSmallUsersList());
        smallUserRepository.addAll(getBigUsersList());
        bigUserRepository.addAll(new ArrayList<>());

        val smallUsersListSize = getSmallUsersList().size();
        val bigUsersListSize = getBigUsersList().size();

        assertAll(
                () -> assertEquals(smallUsersListSize, emptyUserRepository.size()),
                () -> assertEquals(bigUsersListSize, smallUserRepository.size()),
                () -> assertEquals(0, bigUserRepository.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find All Test")
    void findAll() {
        @NotNull val emptyRepoAllUsers = emptyUserRepository.findAll();
        @NotNull val smallRepoAllUsers = smallUserRepository.findAll();
        @NotNull val bigRepoAllUsers = bigUserRepository.findAll();

        assertAll(
                () -> assertEquals(0, emptyRepoAllUsers.size()),
                () -> assertEquals(100, smallRepoAllUsers.size()),
                () -> assertEquals(10000, bigRepoAllUsers.size())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Id Test")
    void findById() {
        @NotNull val newUser = new User();
        @NotNull val userId = newUser.getId();

        smallUserRepository.create(newUser);
        @Nullable val emptyRepoFoundedUser = emptyUserRepository.findById(userId);
        @Nullable val smallRepoFoundedUser = smallUserRepository.findById(userId);

        assertAll(
                () -> assertNull(emptyRepoFoundedUser),
                () -> assertNotNull(smallRepoFoundedUser),
                () -> assertEquals(newUser, smallRepoFoundedUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Clear Test")
    void clear() {
        emptyUserRepository.clear();
        smallUserRepository.clear();
        bigUserRepository.clear();

        assertAll(
                () -> assertTrue(emptyUserRepository.isEmpty()),
                () -> assertTrue(smallUserRepository.isEmpty()),
                () -> assertTrue(bigUserRepository.isEmpty())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Id Test")
    void removeById() {
        @NotNull val newUser = new User();
        @NotNull val userId = newUser.getId();

        smallUserRepository.create(newUser);
        emptyUserRepository.removeById(userId);
        smallUserRepository.removeById(userId);

        @Nullable val emptyRepoFoundedUser = emptyUserRepository.findById(userId);
        @Nullable val smallRepoFoundedUser = smallUserRepository.findById(userId);

        assertAll(
                () -> assertNull(emptyRepoFoundedUser),
                () -> assertNull(smallRepoFoundedUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Login Test")
    void findByLogin() {
        @NotNull val newUser = new User();
        @NotNull val newUserLogin = "Test";
        newUser.setLogin(newUserLogin);

        smallUserRepository.create(newUser);
        @Nullable val emptyRepoFoundedUser = emptyUserRepository.findByLogin(newUserLogin);
        @Nullable val smallRepoFoundedUser = smallUserRepository.findByLogin(newUserLogin);

        assertAll(
                () -> assertNull(emptyRepoFoundedUser),
                () -> assertNotNull(smallRepoFoundedUser),
                () -> assertEquals(newUser, smallRepoFoundedUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Find By Email Test")
    void findByEmail() {
        @NotNull val newUser = new User();
        @NotNull val newUserEmail = "test@test.com";
        newUser.setEmail(newUserEmail);

        smallUserRepository.create(newUser);
        @Nullable val emptyRepoFoundedUser = emptyUserRepository.findByEmail(newUserEmail);
        @Nullable val smallRepoFoundedUser = smallUserRepository.findByEmail(newUserEmail);

        assertAll(
                () -> assertNull(emptyRepoFoundedUser),
                () -> assertNotNull(smallRepoFoundedUser),
                () -> assertEquals(newUser, smallRepoFoundedUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit Password Test")
    void editPassword() {
        @NotNull val newUser = new User();
        @NotNull val newUserId = newUser.getId();
        @NotNull val newUserPassword = "qwerty";
        newUser.setHashedPassword(newUserPassword);

        @Nullable val clonedNewUser = newUser.clone();
        assertNotNull(clonedNewUser);
        smallUserRepository.create(clonedNewUser);

        @NotNull val editedNewUserPassword = "ytrewq";
        @Nullable val emptyRepoEditedPassUser = emptyUserRepository.editPassword(newUserId, editedNewUserPassword);
        @Nullable val smallRepoEditedPassUser = smallUserRepository.editPassword(newUserId, editedNewUserPassword);

        assertAll(
                () -> assertNull(emptyRepoEditedPassUser),
                () -> assertNotNull(smallRepoEditedPassUser),
                () -> assertNotEquals(newUser, clonedNewUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Edit Info Test")
    void editUserInfo() {
        @NotNull val newUser = new User();
        @NotNull val newUserId = newUser.getId();
        @NotNull val newUserFirstname = "Test";
        @NotNull val newUserLastname = "Test";
        newUser.setFirstName(newUserFirstname);
        newUser.setLastName(newUserLastname);

        smallUserRepository.create(newUser);

        @NotNull val newFirstname = "Admin";
        @NotNull val newLastname = "Admin";

        @Nullable val emptyRepoEditedUser =
                emptyUserRepository.editUserInfo(newUserId, newUserFirstname, newUserLastname);
        @Nullable val smallRepoEditedUser =
                smallUserRepository.editUserInfo(newUserId, newUserFirstname, newUserLastname);

        assertNotNull(smallRepoEditedUser);
        assertAll(
                () -> assertNull(emptyRepoEditedUser),
                () -> assertNotEquals(newFirstname, smallRepoEditedUser.getFirstName()),
                () -> assertNotEquals(newLastname, smallRepoEditedUser.getLastName())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Remove By Login Test")
    void deleteByLogin() {
        @NotNull val newUser = new User();
        @NotNull val newUserLogin = "Test";

        smallUserRepository.create(newUser);
        emptyUserRepository.deleteByLogin(newUserLogin);
        smallUserRepository.deleteByLogin(newUserLogin);

        @Nullable val emptyRepoFoundedUser = emptyUserRepository.findByLogin(newUserLogin);
        @Nullable val smallRepoFoundedUser = smallUserRepository.findById(newUserLogin);

        assertAll(
                () -> assertNull(emptyRepoFoundedUser),
                () -> assertNull(smallRepoFoundedUser)
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Lock/Unlock By Id Test")
    void lockUnlockById() {
        @NotNull val newUser = new User();
        @NotNull val newUserId = newUser.getId();

        smallUserRepository.create(newUser);
        emptyUserRepository.lockUnlockById(newUserId);
        smallUserRepository.lockUnlockById(newUserId);

        @Nullable val emptyRepoFoundedLockedUser = emptyUserRepository.findById(newUserId);
        @Nullable val smallRepoFoundedLockedUser = smallUserRepository.findById(newUserId);

        assertNotNull(smallRepoFoundedLockedUser);
        assertAll(
                () -> assertNull(emptyRepoFoundedLockedUser),
                () -> assertTrue(smallRepoFoundedLockedUser.isLocked())
        );

        emptyUserRepository.lockUnlockById(newUserId);
        smallUserRepository.lockUnlockById(newUserId);

        @Nullable val emptyRepoFoundedUnlockedUser = emptyUserRepository.findById(newUserId);
        @Nullable val smallRepoFoundedUnlockedUser = smallUserRepository.findById(newUserId);

        assertNotNull(smallRepoFoundedUnlockedUser);
        assertAll(
                () -> assertNull(emptyRepoFoundedUnlockedUser),
                () -> assertFalse(smallRepoFoundedUnlockedUser.isLocked())
        );
    }

    @Test
    @Tag("Unit")
    @DisplayName("Lock/Unlock By Id Test")
    void lockUnlockByLogin() {
        @NotNull val newUser = new User();
        @NotNull val newUserLogin = "Test";
        newUser.setLogin(newUserLogin);

        smallUserRepository.create(newUser);
        emptyUserRepository.lockUnlockByLogin(newUserLogin);
        smallUserRepository.lockUnlockByLogin(newUserLogin);

        @Nullable val emptyRepoFoundedLockedUser = emptyUserRepository.findByLogin(newUserLogin);
        @Nullable val smallRepoFoundedLockedUser = smallUserRepository.findByLogin(newUserLogin);

        assertNotNull(smallRepoFoundedLockedUser);
        assertAll(
                () -> assertNull(emptyRepoFoundedLockedUser),
                () -> assertTrue(smallRepoFoundedLockedUser.isLocked())
        );

        emptyUserRepository.lockUnlockByLogin(newUserLogin);
        smallUserRepository.lockUnlockByLogin(newUserLogin);

        @Nullable val emptyRepoFoundedUnlockedUser = emptyUserRepository.findByLogin(newUserLogin);
        @Nullable val smallRepoFoundedUnlockedUser = smallUserRepository.findByLogin(newUserLogin);

        assertNotNull(smallRepoFoundedUnlockedUser);
        assertAll(
                () -> assertNull(emptyRepoFoundedUnlockedUser),
                () -> assertFalse(smallRepoFoundedUnlockedUser.isLocked())
        );
    }

}