package tsc.abzalov.tm.repository;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class ProjectRepositoryTest {

    @Test
    @Tag("Unit")
    @DisplayName("Context Test")
    void context() {
        @NotNull val projectRepository = new ProjectRepository();
        assertNotNull(projectRepository);
    }

}