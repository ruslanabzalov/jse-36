package tsc.abzalov.tm.command.auth;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;


public final class AuthRegisterCommand extends AbstractCommand {

    public AuthRegisterCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "register";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Register new user.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("CREATE USER");
        @NotNull val login = inputLogin();
        @NotNull val password = inputPassword();
        @NotNull val firstName = inputFirstName();
        @Nullable val lastName = inputLastName();
        @NotNull val email = inputEmail();

        @NotNull val adminEndpoint = getServiceLocator().getAdminEndpoint();
        adminEndpoint.adminCreateUser(getServiceLocator().getSession(), login, password, firstName, lastName, email);
        System.out.println("User was successfully registered.\n");
    }

}
