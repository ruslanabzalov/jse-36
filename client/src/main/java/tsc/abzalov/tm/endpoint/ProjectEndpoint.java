package tsc.abzalov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.3
 * 2021-04-06T08:28:20.078+03:00
 * Generated source version: 3.4.3
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.abzalov.tsc/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/createProjectRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/createProjectResponse")
    @RequestWrapper(localName = "createProject", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.CreateProject")
    @ResponseWrapper(localName = "createProjectResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.CreateProjectResponse")
    public void createProject(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByNameRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByNameResponse")
    @RequestWrapper(localName = "removeProjectByName", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectByName")
    @ResponseWrapper(localName = "removeProjectByNameResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectByNameResponse")
    public void removeProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/addAllProjectsRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/addAllProjectsResponse")
    @RequestWrapper(localName = "addAllProjects", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.AddAllProjects")
    @ResponseWrapper(localName = "addAllProjectsResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.AddAllProjectsResponse")
    public void addAllProjects(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "projects", targetNamespace = "")
        java.util.List<tsc.abzalov.tm.endpoint.Project> projects
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/areProjectsEmptyRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/areProjectsEmptyResponse")
    @RequestWrapper(localName = "areProjectsEmpty", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.AreProjectsEmpty")
    @ResponseWrapper(localName = "areProjectsEmptyResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.AreProjectsEmptyResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean areProjectsEmpty(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByIndexRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByIndexResponse")
    @RequestWrapper(localName = "removeProjectByIndex", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectByIndex")
    @ResponseWrapper(localName = "removeProjectByIndexResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectByIndexResponse")
    public void removeProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        int index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByNameRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByNameResponse")
    @RequestWrapper(localName = "sortProjectsByName", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByName")
    @ResponseWrapper(localName = "sortProjectsByNameResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> sortProjectsByName(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/clearProjectsRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/clearProjectsResponse")
    @RequestWrapper(localName = "clearProjects", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ClearProjects")
    @ResponseWrapper(localName = "clearProjectsResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ClearProjectsResponse")
    public void clearProjects(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByIndexRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByIndexResponse")
    @RequestWrapper(localName = "editProjectByIndex", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectByIndex")
    @ResponseWrapper(localName = "editProjectByIndexResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project editProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        int index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/projectsSizeRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/projectsSizeResponse")
    @RequestWrapper(localName = "projectsSize", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ProjectsSize")
    @ResponseWrapper(localName = "projectsSizeResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ProjectsSizeResponse")
    @WebResult(name = "return", targetNamespace = "")
    public long projectsSize(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findAllProjectsResponse")
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindAllProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> findAllProjects(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByStatusRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByStatusResponse")
    @RequestWrapper(localName = "sortProjectsByStatus", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByStatus")
    @ResponseWrapper(localName = "sortProjectsByStatusResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByStatusResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> sortProjectsByStatus(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/clearAllProjectsRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/clearAllProjectsResponse")
    @RequestWrapper(localName = "clearAllProjects", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ClearAllProjects")
    @ResponseWrapper(localName = "clearAllProjectsResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ClearAllProjectsResponse")
    public void clearAllProjects(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByIdResponse")
    @RequestWrapper(localName = "editProjectById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectById")
    @ResponseWrapper(localName = "editProjectByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project editProjectById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByIndexRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByIndexResponse")
    @RequestWrapper(localName = "findProjectByIndex", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectByIndex")
    @ResponseWrapper(localName = "findProjectByIndexResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project findProjectByIndex(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        int index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectsByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectsByIdResponse")
    @RequestWrapper(localName = "findProjectsById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectsById")
    @ResponseWrapper(localName = "findProjectsByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectsByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project findProjectsById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByIdResponse")
    @RequestWrapper(localName = "findProjectById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectById")
    @ResponseWrapper(localName = "findProjectByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project findProjectById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByNameRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/editProjectByNameResponse")
    @RequestWrapper(localName = "editProjectByName", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectByName")
    @ResponseWrapper(localName = "editProjectByNameResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EditProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project editProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByNameRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findProjectByNameResponse")
    @RequestWrapper(localName = "findProjectByName", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectByName")
    @ResponseWrapper(localName = "findProjectByNameResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project findProjectByName(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByEndDateRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByEndDateResponse")
    @RequestWrapper(localName = "sortProjectsByEndDate", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByEndDate")
    @ResponseWrapper(localName = "sortProjectsByEndDateResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByEndDateResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> sortProjectsByEndDate(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByStartDateRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sortProjectsByStartDateResponse")
    @RequestWrapper(localName = "sortProjectsByStartDate", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByStartDate")
    @ResponseWrapper(localName = "sortProjectsByStartDateResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SortProjectsByStartDateResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> sortProjectsByStartDate(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/removeProjectByIdResponse")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.RemoveProjectByIdResponse")
    public void removeProjectById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findAllProjectsByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/findAllProjectsByIdResponse")
    @RequestWrapper(localName = "findAllProjectsById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindAllProjectsById")
    @ResponseWrapper(localName = "findAllProjectsByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.FindAllProjectsByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<tsc.abzalov.tm.endpoint.Project> findAllProjectsById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/startProjectByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/startProjectByIdResponse")
    @RequestWrapper(localName = "startProjectById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.StartProjectById")
    @ResponseWrapper(localName = "startProjectByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.StartProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project startProjectById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/projectIndexRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/projectIndexResponse")
    @RequestWrapper(localName = "projectIndex", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ProjectIndex")
    @ResponseWrapper(localName = "projectIndexResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.ProjectIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public int projectIndex(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "entity", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Project entity
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/endProjectByIdRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/endProjectByIdResponse")
    @RequestWrapper(localName = "endProjectById", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EndProjectById")
    @ResponseWrapper(localName = "endProjectByIdResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.EndProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public tsc.abzalov.tm.endpoint.Project endProjectById(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sizeProjectsRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/sizeProjectsResponse")
    @RequestWrapper(localName = "sizeProjects", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SizeProjects")
    @ResponseWrapper(localName = "sizeProjectsResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.SizeProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public long sizeProjects(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/isEmptyProjectListRequest", output = "http://endpoint.tm.abzalov.tsc/ProjectEndpoint/isEmptyProjectListResponse")
    @RequestWrapper(localName = "isEmptyProjectList", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.IsEmptyProjectList")
    @ResponseWrapper(localName = "isEmptyProjectListResponse", targetNamespace = "http://endpoint.tm.abzalov.tsc/", className = "tsc.abzalov.tm.endpoint.IsEmptyProjectListResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isEmptyProjectList(

        @WebParam(name = "session", targetNamespace = "")
        tsc.abzalov.tm.endpoint.Session session
    );
}
