package tsc.abzalov.tm.api.service.property;

import org.jetbrains.annotations.NotNull;

public interface IHashingPropertyService {

    @NotNull
    String getPasswordSaltProperty();

    @NotNull
    Integer getPasswordCounterProperty();

    @NotNull
    String getSessionSaltProperty();

    @NotNull
    Integer getSessionCounterProperty();

}
