package tsc.abzalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.property.IApplicationPropertyService;
import tsc.abzalov.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IApplicationPropertyService getPropertyService();

    @NotNull
    AdminEndpoint getAdminEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    FileBackupEndpoint getFileBackupEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    ProjectTaskEndpoint getProjectTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

}
