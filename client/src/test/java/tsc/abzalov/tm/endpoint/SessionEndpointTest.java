package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;

import static org.junit.jupiter.api.Assertions.*;

class SessionEndpointTest {

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test
    @Tag("Integration")
    @DisplayName("Close Session Test")
    void closeSession() {
        @Nullable val openedSession = sessionEndpoint.openSession(LOGIN, PASSWORD);
        assertNotNull(openedSession);
        sessionEndpoint.closeSession(openedSession);
        assertThrows(
                WebServiceException.class,
                () -> sessionEndpoint.findSession(openedSession)
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Session Test")
    void findSession() {
        @Nullable val openedSession = sessionEndpoint.openSession(LOGIN, PASSWORD);
        assertNotNull(openedSession);
        assertEquals(openedSession.getUserId(), sessionEndpoint.findSession(openedSession).getUserId());
    }

    @Test
    @Tag("Integration")
    @DisplayName("Open Session Test")
    void openSession() {
        @Nullable val openedSession = sessionEndpoint.openSession(LOGIN, PASSWORD);
        assertNotNull(openedSession);
    }

}