package tsc.abzalov.tm.endpoint;

import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

// TODO: !!!.
class ProjectTaskEndpointTest {

    @NotNull
    private static final String LOGIN = "admin";

    @NotNull
    private static final String PASSWORD = "admin";

    @NotNull
    private static final String PROJECT_NAME = "Testing Project Name";

    @NotNull
    private static final String PROJECT_DESCRIPTION = "Testing Project Description";

    @NotNull
    private static final String TASK_NAME = "Testing Task Name";

    @NotNull
    private static final String TASK_DESCRIPTION = "Testing Task Description";

    @NotNull
    private static final String TEST_TASK_NAME = "Test Task Name";

    @NotNull
    private static final String TEST_TASK_DESCRIPTION = "Test Task Description";

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();

    @Nullable
    private Session session;

    @NotNull
    private final List<Project> projects = new ArrayList<>();

    @NotNull
    private final List<Task> tasks = new ArrayList<>();

    @BeforeEach
    void setUp() {
        session = sessionEndpoint.openSession(LOGIN, PASSWORD);
        if (session != null) {
            @Nullable Project project;
            @Nullable Task task;
            for (int i = 0; i < 10; i++) {
                projectEndpoint.createProject(session, PROJECT_NAME + i, PROJECT_DESCRIPTION + i);
                taskEndpoint.createTask(session, TASK_NAME + i, TASK_DESCRIPTION + i);
                project = projectEndpoint.findProjectByName(session, PROJECT_NAME + i);
                projects.add(project);
                task = taskEndpoint.findTaskByName(session, TASK_NAME + i);
                tasks.add(task);

                if (project != null && task != null)
                    projectTaskEndpoint.addTaskToProjectById(session, project.getId(), task.getId());
            }
        }
    }

    @AfterEach
    void tearDown() {
        if (session != null) {
            projectEndpoint.clearProjects(session);
            taskEndpoint.clearTasks(session);
            sessionEndpoint.closeSession(session);
            projects.clear();
            tasks.clear();
        }
    }

    @Test
    @Tag("Integration")
    @DisplayName("Delete Project By Id Test")
    void deleteProjectById() {
        @NotNull val someProject = projects.get(3);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.deleteProjectById(null, someProject.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.deleteProjectById(session, null)
                ),
                () -> {
                    assertTrue(projectTaskEndpoint.hasData(session));
                    projectEndpoint.clearProjects(session);
                    assertFalse(projectTaskEndpoint.hasData(session));
                    taskEndpoint.clearTasks(session);
                    assertFalse(projectTaskEndpoint.hasData(session));
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project Tasks By Id Test")
    void findProjectTasksById() {
        @NotNull val someProject = projects.get(3);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findProjectTasksById(null, someProject.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findProjectTasksById(session, null)
                ),
                () -> assertNotNull(projectTaskEndpoint.findProjectTasksById(session, someProject.getId()))
        );
    }

    // TODO: Обратить внимание.
    @Test
    @Tag("Integration")
    @DisplayName("Add Task To Project By Id Test")
    void addTaskToProjectById() {
        @NotNull val someProject = projects.get(0);
        taskEndpoint.createTask(session, TEST_TASK_NAME, TEST_TASK_DESCRIPTION);
        @NotNull val newTask = taskEndpoint.findTaskByName(session, TEST_TASK_NAME);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.addTaskToProjectById(null, someProject.getId(), newTask.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.addTaskToProjectById(this.session, null, newTask.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.addTaskToProjectById(this.session, someProject.getId(), null)
                )
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Task Project By Id Test")
    void findTaskProjectById() {
        @NotNull val someProject = projects.get(3);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findTaskProjectById(null, someProject.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findTaskProjectById(session, null)
                ),
                () -> assertNotNull(projectTaskEndpoint.findTaskProjectById(session, someProject.getId()))
        );
    }

    // TODO: Обратить внимание.
    @Test
    @Tag("Integration")
    @DisplayName("Delete Project Task By Id Test")
    void deleteProjectTaskById() {
        @NotNull val someProject = projects.get(3);
        taskEndpoint.createTask(session, TEST_TASK_NAME, TEST_TASK_DESCRIPTION);
        @NotNull val newTask = taskEndpoint.findTaskByName(session, TEST_TASK_NAME);
        projectTaskEndpoint.addTaskToProjectById(session, someProject.getId(), newTask.getId());
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.deleteProjectTaskById(null, someProject.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.deleteProjectTaskById(this.session, null)
                ),
                () -> {
                    projectTaskEndpoint.deleteProjectTaskById(this.session, someProject.getId());
                    @Nullable val task = projectTaskEndpoint.findProjectTaskById(this.session, newTask.getId());
                    assertNotNull(task);
                    assertNull(task.getProjectId());
                }
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Find Project Task By Id Test")
    void findProjectTaskById() {
        @NotNull val someTask = tasks.get(3);
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findProjectTaskById(null, someTask.getId())
                ),
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.findProjectTaskById(session, null)
                ),
                () -> assertNotNull(projectTaskEndpoint.findProjectTaskById(session, someTask.getId()))
        );
    }

    @Test
    @Tag("Integration")
    @DisplayName("Has Data Test")
    void hasData() {
        assertAll(
                () -> assertThrows(
                        WebServiceException.class,
                        () -> projectTaskEndpoint.hasData(null)
                ),
                () -> {
                    assertTrue(projectTaskEndpoint.hasData(session));
                    projectEndpoint.clearProjects(session);
                    assertFalse(projectTaskEndpoint.hasData(session));
                    taskEndpoint.clearTasks(session);
                    assertFalse(projectTaskEndpoint.hasData(session));
                }
        );
    }

}